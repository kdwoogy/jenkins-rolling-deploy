OUTPUT_DIR=$1
OUT_TG_FILE=$OUTPUT_DIR/result/1.target_group_name.txt
BAK_OUT_TG_FILE=$OUTPUT_DIR/result/bak/1.target_group_name.txt

echo " "
echo " "
echo " "
echo "##################################################"
echo "#####    1.Target Group명 획득                #####"
echo "##################################################"
echo " "

echo "OUTPUT_DIR is $OUTPUT_DIR"
echo "OUT_TG_FILE is $OUT_TG_FILE"
echo "BAK_OUT_TG_FILE is $BAK_OUT_TG_FILE"

if [ -z $1 ]; then
        echo "OUTPUT_DIR isempty!!"
        echo "usage) 1.get_target_group.sh [OUPUT_DIR] [ALB ARN]"
        exit 1
fi

if [ -z $2 ]; then
        echo "ALB ARN is empty!!"
        echo "usage) 1.get_target_group.sh [OUPUT_DIR] [ALB ARN]"
        exit 1
fi

mv $OUT_TG_FILE $BAK_OUT_TG_FILE

aws elbv2 describe-target-groups --load-balancer-arn $2 | jq -r ".TargetGroups[].TargetGroupArn" > $OUT_TG_FILE
