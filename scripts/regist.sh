TG_MEMBER_STATUS="status"
MAX_COUNTS=$3
IPADDRESS=`aws ec2 describe-instances --instance-id $2 | jq -r .Reservations[].Instances[].PrivateIpAddress`

# resister targets
echo "resister targets"
aws elbv2 register-targets --target-group-arn $1 --targets Id=$2
echo "register $IPADDRESS started!!"

