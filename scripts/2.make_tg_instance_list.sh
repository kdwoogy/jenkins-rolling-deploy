OUTPUT_DIR=$1
FILE_NAME=$OUTPUT_DIR/result/1.target_group_name.txt
INSTANCE_OUT_NAME=$OUTPUT_DIR/result/2.instance_list.txt
BAK_INSTANCE_OUT_NAME=$OUTPUT_DIR/result/bak/2.instance_list.txt
TARGET_GROUP_COUNT=`cat $FILE_NAME | wc -l`

echo " "
echo " "
echo " "
echo "##################################################"
echo "#####    2. Instance List 생성                #####"
echo "##################################################"
echo " "

if [ -z $1 ]; then
        echo "OUTPUT_DIR is empty!!"
        echo "usage) 2.make_tg_instance_list.sh [OUPUT_DIR] [TARGET_GROUP_NAME]"
        exit 1
fi

echo $2

# intialize output file
mv $INSTANCE_OUT_NAME $BAK_INSTANCE_OUT_NAME

if [ ! -f $FILE_NAME ]; then
        echo "$FILE_NAME file not found!!"
        exit 1
fi

if [ ! -s $FILE_NAME ]; then
        echo "$FILE_NAME file is empty!!"
        exit 1
fi
# 만약 ALB에 포한된 target group이 1개 이상이라 사용할 수 없고 target group 파마미터도 없다면 파마리터로 지정하도록 요청
if [ "$TARGET_GROUP_COUNT" -gt "1" ] && [ -z $2 ]; then
        echo "ALB has more than one target group. Please add specific target group ARN for this script parameter!!"
        exit 1
# target group 파라미터가 있으면 그걸 target group 리스트 파일에 저장
elif [ ! -z $2 ]; then
        echo $2 > $FILE_NAME
fi
        TG_NAME=$(<$FILE_NAME)

aws elbv2 describe-target-health --target-group-arn $TG_NAME | jq -r ".TargetHealthDescriptions[].Target.Id" > $INSTANCE_OUT_NAME
exit 0