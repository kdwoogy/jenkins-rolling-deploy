OUTPUT_FILE=$1
EVEN_FILE_NAME=$OUTPUT_FILE/result/3.even_instance_list.txt
ODD_FILE_NAME=$OUTPUT_FILE/result/3.odd_instance_list.txt
TG_NAME=$(<$OUTPUT_FILE/result/1.target_group_name.txt)

echo " "
echo " "
echo " "
echo "##################################################"
echo "#####    $2 Instance ALB Regist 작업         #####"
echo "##################################################"
echo " "

if [ -z $1 ]; then
        echo "OUTPUT_DIR parameter is missing!!"
        echo "usage) 6.regist_instance.sh [OUTPUT_DIR] [EVEN / ODD]"
        exit 1
fi

if [ -z $2 ]; then
        echo "target instances are empty!!"
        echo "usage) 6.regist_instance.sh [OUTPUT_DIR] [EVEN / ODD]"
        exit 1
fi

if [ $2 == "EVEN" ]; then
        if [ ! -f $EVEN_FILE_NAME ]; then
                echo "$EVEN_FILE_NAME file not found!!"
                exit 1
        fi
        INSTANCE_LISTS=$(<$EVEN_FILE_NAME)
elif [ $2 == "ODD" ]; then
        if [ ! -f $ODD_FILE_NAME ]; then
                echo "$ODD_FILE_NAME file not found!!"
                exit 1
        fi
        INSTANCE_LISTS=$(<$ODD_FILE_NAME)
else
        echo "wrong usage!!"
        echo "usage) 6.regist_instance.sh [OUTPUT_DIR] [EVEN / ODD]"
        exit 1
fi

if [ -z "$INSTANCE_LISTS" ]; then
        echo "Instacne List is empty!!"
        exit 1
fi

# regist Instance
for instance in $INSTANCE_LISTS; do
	      /home/ec2-user/scripts/regist.sh $TG_NAME $instance
	      echo "Regist Instance $instance started!!"
done

# regist Instance check
for instance in $INSTANCE_LISTS; do
        /home/ec2-user/scripts/regist_check.sh $TG_NAME $instance 100
        if [ $? == 1 ]; then
              exit 1
        fi
  echo "Regist Instance $instance completed!!"
done
