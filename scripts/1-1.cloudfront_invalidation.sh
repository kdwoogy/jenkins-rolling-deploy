OUTPUT_FILE=$1
DISTRIBUTION_ID=$2
TARGET_PATH=$3
INVALIDATION_ID_FILE_NAME=$OUTPUT_FILE/result/1-1.invalidation_id.txt
COUNTS=0
MAX_COUNTS=100
INVALIDATION_STATUS="NotReady"

echo " "
echo " "
echo " "
echo "##################################################"
echo "#####   CloudFront Invalidation 작업       #####"
echo "##################################################"
echo " "

if [ -z "$1" ]; then
        echo "OUTPUT_DIR parammeter is missing!!"
        echo "usage) 1-1.cloudfront_invalidation.sh [OUTPUT_DIR] [DISTRIBUTION_ID] [TARGET_PATH]"
        exit 1
fi

if [ -z "$2" ]; then
        echo "DISTRIBUTION_ID parammeter is missing!!"
        echo "usage) 1-1.cloudfront_invalidation.sh [OUTPUT_DIR] [DISTRIBUTION_ID] [TARGET_PATH]"
        exit 1
fi

if [ -z "$3" ]; then
        echo "TARGET_PATH parammeter is missing!!"
        echo "usage) 1-1.cloudfront_invalidation.sh [OUTPUT_DIR] [DISTRIBUTION_ID] [TARGET_PATH]"
        exit 1
fi

# CloudFront invalidation Start
mv $OUTPUT_FILE/result/1-1.invalidation_id.txt $OUTPUT_FILE/result/bak/1-1.invalidation_id.txt

aws cloudfront create-invalidation --distribution-id $DISTRIBUTION_ID --paths "$TARGET_PATH"|jq -r .Invalidation.Id > $INVALIDATION_ID_FILE_NAME

if [ $? == 1 ]; then
              echo "CloudFront Invalidation Failed"
              exit 1
fi

# IF Invalidation ID is empty
if [ ! -s $INVALIDATION_ID_FILE_NAME ]; then
        echo "$INVALIDATION_ID_FILE_NAME file is empty!!"
        exit 1
fi

INVALIDATION_ID=$(<$INVALIDATION_ID_FILE_NAME)

echo "CloudFront Invalidation Started !!!"
echo "================================"

# CloudFront invalidation status check

until [ ${INVALIDATION_STATUS} = "Completed" ]
do

INVALIDATION_STATUS=`aws cloudfront get-invalidation --distribution-id $DISTRIBUTION_ID --id $INVALIDATION_ID|jq -r .Invalidation.Status`

COUNTS=$((${COUNTS}+1))

echo "CloudFront Invalidation $INVALIDATION_ID status is ${INVALIDATION_STATUS}, waiting until status is Completed $COUNTS"

# too many try to check status --> break
if [ $COUNTS -eq $MAX_COUNTS ]
then
        echo "CloudFront Invalidation Failled!!!, too many try to check status more than $COUNTS"
	aws cloudfront get-invalidation --distribution-id $DISTRIBUTION_ID --id $INVALIDATION_ID
	exit 1
fi

sleep 2
done

if [ "$INVALIDATION_STATUS" = "Completed" ] ;
then
	aws cloudfront get-invalidation --distribution-id $DISTRIBUTION_ID --id $INVALIDATION_ID
	echo "========================>  "
	echo "CloudFront Invalidation $INVALIDATION_ID Success!!"
	exit 0
fi