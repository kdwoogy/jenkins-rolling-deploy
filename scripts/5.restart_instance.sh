OUTPUT_DIR=$1
EVEN_FILE_NAME=$OUTPUT_DIR/result/3.even_instance_list.txt
ODD_FILE_NAME=$OUTPUT_DIR/result/3.odd_instance_list.txt
PEM_KEY=/home/ec2-user/.ssh/jn-live-was-group-kp.pem
HEALTHCHECK_ADDRESS=$3
BUILD_FILE_NAME=$4
TARGET_FILE_NAME=$5
INSTANCE_USER=$6

echo " "
echo " "
echo " "
echo "##################################################"
echo "#####    $2 Instance WAS restart 작업        #####"
echo "##################################################"
echo " "

if [ -z $1 ]; then
        echo "OUTPUT_DIR parameter is missing!!"
        echo "usage) 5.restart_instance.sh [OUTPUT_DIR] [EVEN / ODD] [healthcheck url] [build_file_name] [target_file_name] [instance_user]"
        exit 1
fi

if [ -z $2 ]; then
        echo "target instances are empty!!"
        echo "usage) 5.restart_instance.sh [OUTPUT_DIR] [EVEN / ODD] [healthcheck url] [build_file_name] [target_file_name] [instance_user]"
        exit 1
fi

if [ -z $3 ]; then
        echo "healthcheck url is empty!!"
        echo "usage) 5.restart_instance.sh [OUTPUT_DIR] [EVEN / ODD] [healthcheck url] [build_file_name] [target_file_name] [instance_user]"
        exit 1
fi

if [ -z $4 ]; then
        echo "build file name is empty!!"
        echo "usage) 5.restart_instance.sh [OUTPUT_DIR] [EVEN / ODD] [healthcheck url] [build_file_name] [target_file_name] [instance_user]"
        exit 1
fi

if [ -z $5 ]; then
        echo "target file name is empty!!"
        echo "usage) 5.restart_instance.sh [OUTPUT_DIR] [EVEN / ODD] [healthcheck url] [build_file_name] [target_file_name] [instance_user]"
        exit 1
fi

if [ $2 == "EVEN" ]; then
        if [ ! -f $EVEN_FILE_NAME ]; then
                echo "$EVEN_FILE_NAME file not found!!"
                exit 1
        fi
        INSTANCE_LISTS=$(<$EVEN_FILE_NAME)
elif [ $2 == "ODD" ]; then
        if [ ! -f $ODD_FILE_NAME ]; then
                echo "$ODD_FILE_NAME file not found!!"
                exit 1
        fi
        INSTANCE_LISTS=$(<$ODD_FILE_NAME)
else
        echo "wrong usage!!"
        echo "usage) 5.restart_instance.sh [OUTPUT_DIR] [EVEN / ODD] [healthcheck url] [build_file_name] [target_file_name] [instance_user]"
        exit 1
fi

if [ -z "$INSTANCE_LISTS" ]; then
	echo "Instacne List is empty!!"
        exit 1
fi

# set default instance_user
if [ -z $6 ]; then
        INSTANCE_USER=centos
fi

# Deploy service!!
for instance in $INSTANCE_LISTS; do
	IPADDRESS=`aws ec2 describe-instances --instance-id $instance | jq -r .Reservations[].Instances[].PrivateIpAddress`
	echo "Copy Build file to Instance $instance!!"
	scp -i $PEM_KEY $BUILD_FILE_NAME $INSTANCE_USER@$IPADDRESS:$TARGET_FILE_NAME
	if [ $? == 1 ]; then
		echo "Build file '$BUILD_FILE_NAME' is not exist or OS User '$INSTANCE_USER' is a wrong user. Please check scripts!!"
		exit 1
	fi
	echo "Restart Instance $instance!!"
	ssh -i $PEM_KEY $INSTANCE_USER@$IPADDRESS "sh deploy.sh" &
done

# Instance health check!!
for instance in $INSTANCE_LISTS; do
        IPADDRESS=`aws ec2 describe-instances --instance-id $instance | jq -r .Reservations[].Instances[].PrivateIpAddress`
        echo "Healthcheck Instance $instance!!"
	ssh -i $PEM_KEY $INSTANCE_USER@$IPADDRESS "sh ./healthcheck.sh $HEALTHCHECK_ADDRESS"
	if [ $? == 1 ]; then
		echo "health check $IPADDRESS is NotOK!!"
		STATUS=NotOK
		break
	fi
done

if [ "$STATUS" == "NotOK" ]; then
  echo "Monitor failed, website seems to be down."
  exit 1
else
  echo "Monitor finished, website is online."
  exit 0
fi
