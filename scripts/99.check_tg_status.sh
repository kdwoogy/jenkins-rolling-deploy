OUTPUT_DIR=$1
FILE_NAME=$OUTPUT_DIR/result/1.target_group_name.txt
INSTANCE_OUT_NAME=$OUTPUT_DIR/result/2.instance_list.txt

echo " "
echo " "
echo " "
echo "##################################################"
echo "#####    $TG_NAME Instance status check             #####"
echo "##################################################"
echo " "

if [ -z $1 ]; then
        echo "OUTPUT_DIR parameter is missing!!"
        echo "usage) 99.check_tg_status.sh [OUPUT_DIR]"
        exit 1
fi

if [ ! -f $FILE_NAME ]; then
        echo "$FILE_NAME file not found!!"
        exit 1
fi

if [ ! -s $FILE_NAME ]; then
        echo "$FILE_NAME file is empty!!"
        exit 1
fi

TG_NAME=$(<$FILE_NAME)

aws elbv2 describe-target-health --target-group-arn $TG_NAME | jq -r ".TargetHealthDescriptions[]" 
