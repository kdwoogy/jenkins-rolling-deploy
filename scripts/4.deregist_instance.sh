OUTPUT_FILE=$1
SCRIPTS_DIR=/home/ec2-user/scripts
EVEN_FILE_NAME=$OUTPUT_FILE/result/3.even_instance_list.txt
ODD_FILE_NAME=$OUTPUT_FILE/result/3.odd_instance_list.txt
TG_NAME=$(<$OUTPUT_FILE/result/1.target_group_name.txt)

echo " "
echo " "
echo " "
echo "##################################################"
echo "#####    $2 Instance List ALB 분리 작업       #####"
echo "##################################################"
echo " "

if [ -z $1 ]; then
        echo "OUTPUT_DIR parammeter is missing!!"
        echo "usage) 4.deregist_instance.sh [OUTPUT_DIR] [EVEN / ODD]"
        exit 1
fi

if [ -z $2 ]; then
        echo "EVEN or ODD parameter is missing!!"
        echo "usage) 4.deregist_instance.sh [OUTPUT_DIR] [EVEN / ODD]"
        exit 1
fi

if [ $2 == "EVEN" ]; then
	if [ ! -f $EVEN_FILE_NAME ]; then
        	echo "$EVEN_FILE_NAME file not found!!"
        	exit 1
	fi
	INSTANCE_LISTS=$(<$EVEN_FILE_NAME)
elif [ $2 == "ODD" ]; then
	if [ ! -f $ODD_FILE_NAME ]; then
                echo "$ODD_FILE_NAME file not found!!"
                exit 1
        fi
	INSTANCE_LISTS=$(<$ODD_FILE_NAME)
else 
	echo "wrong usage!!"
	echo "usage) 4.deregist_instance.sh [OUTPUT_DIR] [EVEN / ODD]"
	exit 1
fi

if [ -z "$INSTANCE_LISTS" ]; then
	echo "Instance List is empty!!"
        exit 1
fi

# deregist Instance
for instance in $INSTANCE_LISTS; do
	$SCRIPTS_DIR/deregist.sh $TG_NAME $instance
	echo "Deregist Instance $instance started!!"
done

# check status of deregisted Instance
for instance in $INSTANCE_LISTS; do
        $SCRIPTS_DIR/deregist_check.sh $TG_NAME $instance 200
        if [ $? == 1 ]; then
              exit 1
        fi
        echo "Deregist Instance $instance completed!!"
done
