TG_MEMBER_STATUS="status"
COUNTS=0
MAX_COUNTS=$3
IPADDRESS=`aws ec2 describe-instances --instance-id $2 | jq -r .Reservations[].Instances[].PrivateIpAddress`

until [ ${TG_MEMBER_STATUS} = "healthy" ]
do
TG_MEMBER_STATUS=`aws elbv2 describe-target-health --target-group-arn $1 --targets Id=$2 |  grep -oP '(?<="State": ")[^"]*'`

COUNTS=$((${COUNTS}+1))

echo "ALB target status is $TG_MEMBER_STATUS $IPADDRESS, waiting until status is healthy $COUNTS"

# too many try to check status --> break
if [ $COUNTS -eq $MAX_COUNTS ]
then
        echo "$IPADDRESS Fail!!!  too many try to check status more than $COUNTS" 
	exit 1
fi

sleep 2
done

if [ "$TG_MEMBER_STATUS" = "healthy" ] ;
then
echo "register $IPADDRESS success!!"
exit 0
fi
