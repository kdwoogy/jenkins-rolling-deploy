TG_MEMBER_STATUS="status"
COUNTS=0
IPADDRESS=`aws ec2 describe-instances --instance-id $2 | jq -r .Reservations[].Instances[].PrivateIpAddress`

# deresister targets
echo "deresister targets"
echo $1 $2
aws elbv2 deregister-targets --target-group-arn $1 --targets Id=$2

echo "ALB target $TG_MEMBER_STATUS $IPADDRESS deresiter started"

