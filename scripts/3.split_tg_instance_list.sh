OUTPUT_DIR=$1
FILE_NAME=$OUTPUT_DIR/result/2.instance_list.txt
INSTANCE_COUNTS=`cat $FILE_NAME|wc -l`

echo " "
echo " "
echo " "
echo "##################################################"
echo "#####    3.Instance List를 2개로 분리생성       #####"
echo "##################################################"
echo " "

if [ -z $1 ]; then
        echo "OUTPUT_DIR is empty!!"
        echo "usage) 3.split_tg_instance_list.sh [OUPUT_DIR]"
        exit 1
fi

if [ ! -f $FILE_NAME ]; then
	echo "$FILE_NAME file not found!!"
	exit 1
fi

if [ ! -s $FILE_NAME ]; then
	echo "$FILE_NAME file is empty!!"
        exit 1
fi

# initailize output files
if [ -f $OUTPUT_DIR/result/3.even_instance_list.txt ]; then
        mv $OUTPUT_DIR/result/3.even_instance_list.txt $OUTPUT_DIR/result/bak/3.even_instance_list.txt
fi

if [ -f $OUTPUT_DIR/result/3.odd_instance_list.txt ]; then
        mv $OUTPUT_DIR/result/3.odd_instance_list.txt $OUTPUT_DIR/result/bak/3.odd_instance_list.txt
fi

if [ $INSTANCE_COUNTS -lt 2 ]; then
	echo " There is only one instance in this Target Group!! "
	echo " You need regist more than 2 instances to deploy service by this scripts!!"
	exit 1
fi

#sed -e '$!n' -e '1!{w $OUTPUT_DIR/result/3.even_instance_list.txt' -e 'd}' $FILE_NAME > $OUTPUT_DIR/result/3.odd_instance_list.txt
sed -n 'p;n' $FILE_NAME > $OUTPUT_DIR/result/3.odd_instance_list.txt
sed -n '1d;p;n' $FILE_NAME > $OUTPUT_DIR/result/3.even_instance_list.txt
