# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Jenkins 롤링 배포 구성을 위한 스크립트

### How do I get set up? ###

* Jenkins 서버에 scripts 폴더 복사 반영
* Jenkins pipeline 스크립트를 Jenkins 프로젝트 생성 시 반영
** pipeline script 수정 필요 사항
** 1. ALB 변수 수정 ( 서비스에서 사용 중인 ALB의 ARN로 변경)
** 2. HEALTH_URL 수정 ( 서비스에서 사용 중인 HEALTHCHECK URL 기입)
** 3. Build를 위한 Git repository 정보 기입 ( url: 'https://bitbucket.org/joongna/init-api' )
